import './App.css'
import React, { useState, useEffect } from 'react'
import Navbar from "./components/Navbar"
import Boards from './components/Boards'
import { Routes, Route } from 'react-router-dom'
import List from './components/List'
import { SnackbarProvider } from 'notistack';

function App() {

  

  return (

    <>
    <SnackbarProvider maxSnack={3} >
      <Navbar />
      <Routes>
        <Route path="/" element={<Boards  />} />
          <Route path="boards/:boardId" element={<List />} />
        <Route path="/boards" element={<Boards  />} > 
        </Route>
      </Routes>
      </SnackbarProvider>
    </>
  )
}

export default App
