import { Box, Button, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import ProgressBar from './ProgressBar';
import { useSnackbar } from 'notistack';
import { fetchCheckItem, addingItem, handlingDeleteItem,toggler } from './Api/Api';

export default function Checkitems({ checkele, cardid }) {

  const [items, setitems] = useState([]);
  const [adding, setadding] = useState(false);
  const [input, setinput] = useState('');
  const [checked, ischecked] = useState(false);
  const { enqueueSnackbar } = useSnackbar()

  useEffect(() => {

    const fetchitems = async () => {

      try {
        const response = await fetchCheckItem(checkele.id)
        setitems([...response]);
      }
      catch (error) {
        enqueueSnackbar(`Failed to  fetching Checkitems : ${error.message}`, { variant: 'error' });
      }
    }
    fetchitems();
  }, [])
  const Delete = async (id) => {
    try {
      const response = await handlingDeleteItem(checkele.id, id)
      const newdata = items.filter((ele) => {
        return ele.id != id
      })
      setitems(newdata);
    }
    catch (error) {
      console.error('Error deleting list:', error);
      enqueueSnackbar(`Failed in  Delete Checkitems : ${error.message}`, { variant: 'error' });
    }
  }
  const handlesubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await addingItem(checkele.id,input)
      setitems([...items, response]);
      setinput('')
      setadding(false);

    }
    catch (error) {
      enqueueSnackbar(`Failed in  Creating Checkitems : ${error.message}`, { variant: 'error' });
    }
  }

  const handleinput = (e) => {
    setinput(e.target.value);
  }
  const handleaddbutton = () => {
    setadding(false);
    setinput('');
  };

  const handleAddcheckClick = () => {
    setadding(true);
  };
  const HandlingCheckstatus = (chec, eleid) => {

    setitems(prev => {
      const itemprev = prev.map(item => {
        if (item.id === eleid) {
          return { ...item, state: chec ? "complete" : "incomplete" };
        } else

          return item;
      });
      return itemprev;
    });
    const sendcheck = async (id) => {
      console.log('Cardid', cardid);
      console.log('Check id', checkele.id);

      try {
        const response = await toggler(cardid,id,chec)
      }
      catch (error) {
        console.log('Error from Checked ', error);
      }
    }
    sendcheck(eleid);
  }
  const handleProgress = () => {
    const totalItems = items.length;
    const checkedItems = items.filter(item => item.state === 'complete').length;
    const progress = (checkedItems / totalItems) * 100;
    return Math.floor(progress);
  }
  return (
    <>
      {
        items.length > 0 ? (
          <ProgressBar handleProgress={handleProgress} />
        ) : null
      }
      <Box>
        {items.length > 0 &&
          items?.map((element) => (
            <li style={{ color: 'black', display: "flex", justifyContent: 'space-between', alignItems: 'center', width: 300, marginLeft: '3rem' }} key={element.id} >
              <input type='checkbox' checked={element.state == 'complete' ? true : false} onChange={(e) => HandlingCheckstatus(e.target.checked, element.id)} />{element.name}<DeleteIcon sx={{ cursor: 'pointer' }} onClick={() => Delete(element.id)} /></li>
          ))
        }
      </Box >
      <Box sx={{ display: 'flex' }}>
        {adding ? (
          <form onSubmit={handlesubmit} sx={{ display: 'flex', flexDirection: 'column', backgroundColor: '#cfd8dc', ml: 10, width: 200, mb: 4 }}>
            <input
              type="text"
              value={input}
              onChange={handleinput} placeholder="Enter Item title"
              style={{ width: '11rem', height: '2rem', color: 'black', border: 'none', padding: '5px', borderRadius: '10px', marginBottom: '5px', marginLeft: '1rem' }}
            />
            <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
              <Button
                variant="contained"
                sx={{ color: '#fff', width: 150, ml: '1rem' }}
                type='submit'

              >
                Add item
              </Button>
              <CloseIcon sx={{ cursor: 'pointer', color: '#607d8b' }} onClick={handleaddbutton} />

            </Typography>

          </form>) : (
          <Button
            variant="contained"
            sx={{
              fontSize: '100', width: 250, mt: 2, color: '#fff', mb: 2

            }}
            onClick={handleAddcheckClick}
          >
            <AddIcon />Add another item
          </Button>
        )}
      </Box>


    </>

  )
}
