import { Box, Button, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react'
import Popover from '@mui/material/Popover';
import CloseIcon from '@mui/icons-material/Close';
import { useSnackbar } from 'notistack';
import axios from 'axios';
import Checkitems from './Checkitems';
import TaskAltIcon from '@mui/icons-material/TaskAlt';
import CircularProgress from '@mui/material/CircularProgress';
import { fetchCheckList,addingCheckList,handlingDeleteCheckList } from './Api/Api';
export default function Checklist({ cardid }) {
  const key = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
  const token = import.meta.env.VITE_REACT_APP_TRELLO_TOKEN;

  const [check, setcheck] = useState([]);
  const [newcheck, setnewcheck] = useState(false);
  const [newcheckname, setnewcheckName] = useState('');
  const [isLoading, setisloading] = useState(true);
  const { enqueueSnackbar } = useSnackbar();


  useEffect(() => {
    const fetchchecklist = async () => {

      try {
        const response = await fetchCheckList(cardid);
        
        // console.log(data, "From check");
        setcheck(response);
        setisloading(false)
      }
      catch (error) {
        console.log('Error in fetching Lists', error);
        setisloading(false)
        enqueueSnackbar(`Failed to  fetch checklist : ${error.message}`, { variant: 'error' });
      }
    }
    fetchchecklist();
  }, [])

  const AddChecklist = async (e) => {
    e.preventDefault()
    try {
      // const response = await axios.post(`https://api.trello.com/1/cards/${cardid}/checklists?key=${key}&token=${token}`)
      const response = await addingCheckList(cardid,newcheckname)
      console.log('Checklist Created Successfully');
      setcheck([...check, response]);

      setnewcheckName('');
      setnewcheck(true);
      setAnchorEl(null);
    }
    catch (err) {
      console.log('Error in creating Checklist - >', err);
    }
  }
  const handleAddcheck = () => {
    // Logic to add the new Checklist
    setnewcheck(false);
    setnewcheckName(''); 
  };

  function handleinput(e) {
    setnewcheckName(e.target.value);
    console.log(e.target.value);
  }


  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const Delete = async (id) => {
    try {
      const response = await handlingDeleteCheckList(id)
      setcheck((prev) => prev.filter(card => card.id !== id))
      console.log('Clicked ');
    }
    catch (error) {
      console.log("Error in deleting Checklist", error);
    }
  }


  return (
    <div>
      <Button variant="outlined" aria-describedby={id} onClick={handleClick} sx={{ mb: 2 }}>Checklist</Button>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        sx={{ marginLeft: 14.5, marginTop: -2 }}
      >
        <form onSubmit={AddChecklist} sx={{ display: 'flex', flexDirection: 'column', backgroundColor: '#cfd8dc', ml: 3, width: 200 }}>
          <input
            type="text"
            placeholder="Enter  title"
            value={newcheckname}
            onChange={handleinput}
            style={{ width: '11rem', height: '2rem', color: 'black', border: 'none', padding: '5px', borderRadius: '10px', marginBottom: '5px' }}
          />
          <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
            <Button
              variant="contained"
              sx={{ color: '#fff', width: 150 }}
              type='submit'   >
              Add Checklist
            </Button>
            <CloseIcon sx={{ cursor: 'pointer', color: '#607d8b' }} onClick={handleClose} />
          </Typography>
        </form>
      </Popover>
      {isLoading ? ((<Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: 300 }}>
        <CircularProgress />
      </Box>)) :
        check.map((element) => (
          <>
            <li style={{ color: 'black', display: "flex", justifyContent: 'space-between', alignItems: 'center', marginBottom: 2 }} key={element.id} >
              <TaskAltIcon sx={{ color: 'grey' }} />{element.name}<Button variant='outline' sx={{ textAlign: 'center' }} onClick={() => Delete(element.id)} >Delete</Button></li>
            <Checkitems checkele={element} cardid={cardid} />
          </>
        ))
      }
    </div>
  )
}
