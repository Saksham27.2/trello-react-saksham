import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { Box } from '@mui/material';
import axios from 'axios';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import { addingBoards } from './Api/Api';




export default function FormDialog({ boards, setBoards }) {
  const key = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
  const token = import.meta.env.VITE_REACT_APP_TRELLO_TOKEN;

  const [boardName, setBoardName] = useState('');
  const [anchorEl, setAnchorEl] = useState(null);


  const handleBoardNameChange = (event) => {
    setBoardName(event.target.value);
  };


  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;


  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      // const res = await axios.post(`https://api.trello.com/1/boards/?name=${boardName}&key=${key}&token=${token}`)
      const res =await addingBoards(boardName)
      console.log('Board created successfully');
      handleClose();
      setBoards([...boards, res]);
      setBoardName('')
    } catch (error) {
      console.error('Error creating board:', error);
    }
  };
  return (
    <>

      <Box sx={{ background:'#212121',width: 400, height: 100, mt: 3, ml: 4, display: 'flex', justifyContent: 'center', alignItems: 'center',borderRadius:5, }}>

        <Button variant="contained" onClick={handleClick} sx={{ width: 200, height: 50,background:'black',color:'white',
        borderRadius:5,
             '&:hover': {
              backgroundColor: 'grey',
              boxShadow: 'none',
              color:'black'
            }, }}>
          Create New Board
        </Button>
      </Box>



      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        sx={{ml:8}}
      >
        <form onSubmit={handleSubmit}>
          <Typography variant='h6'>Title Of the Board</Typography>
 
          <input
              type="text"
              value={boardName}
              onChange={handleBoardNameChange} placeholder="Enter Board title"
              style={{ width: '11rem', height: '2rem', color: 'black', border: 'none', padding: '5px', borderRadius: '10px', marginBottom: '5px' }}
            />
            <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
              <Button
                variant="contained"
                sx={{ color: '#fff', width: 150 }}
                type='submit'

              >
                Add Board
              </Button>
              <Button onClick={handleClose}>Cancel</Button>

            </Typography>
          
          </form>
      </Popover>
    </>
  );
}