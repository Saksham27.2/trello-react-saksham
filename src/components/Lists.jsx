import React, { useState } from 'react'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import Cartitems from './Cartitems'
import { Box, Popover, Typography } from '@mui/material';
import { useSnackbar } from 'notistack';
import { deleteList } from './Api/Api';



export default function Lists({ list, setlists }) {
    const [anchorEl, setAnchorEl] = useState(null);
    const { enqueueSnackbar } = useSnackbar();
    const handleClosePopover = () => {
        setAnchorEl(null);
    };
    const open = Boolean(anchorEl);
    const handleMoreHorizIconClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const DeleteList = async (id) => {
    
        try{
            await deleteList(id);
            setlists((prevData) => prevData.filter((list) => list.id != id));
        }
         catch (err) {
            enqueueSnackbar(`Failed in  Deleting Cards : ${err.message}`, { variant: 'error' });
        }

    }

    return (

                <Card sx={{ ml: 9, mt: 3, minWidth: '300px', backgroundColor: '#cfd8dc', display: 'flex', justifyContent: 'space-between', flexDirection: 'column', height: 'max-content', minHeight: 'inherit', paddingBottom: '2rem' }} key={list.id}>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div" sx={{ display: 'flex', justifyContent: 'space-between' }}>
                            {list.name}
                            <MoreHorizIcon sx={{ cursor: 'pointer' }} onClick={handleMoreHorizIconClick} />
                            <Popover
                                open={open}
                                anchorEl={anchorEl}
                                onClose={handleClosePopover}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                            >
                                {/* Content of the Popover */}
                                <Box sx={{ p: 2 }}>
                                    <Typography onClick={() => {
                                        DeleteList(list.id)
                                        handleClosePopover()
                                    }} sx={{ cursor: 'pointer' }}>Delete List</Typography>
                                </Box>
                            </Popover>
                        </Typography>
                    </CardContent>
                    <Cartitems listid={list.id} />
                </Card>
    )
}
