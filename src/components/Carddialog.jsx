import { Box, Modal, Typography, Button } from '@mui/material';
import React, { useState } from 'react'
import Checklist from './Checklist';
import { deleteCard } from './Api/Api';
import {  useSnackbar } from 'notistack';



const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

export default function Carddialog({ card, setcards }) {

  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const { enqueueSnackbar } = useSnackbar();
  
  let deleteCardbyid = async (cardid) => {
    console.log('Button is clicked');
    try {
      await deleteCard(cardid);
      setcards((prev) => prev.filter(card => card.id !== cardid))
    }
    catch (error) {
      enqueueSnackbar(`Failed to  Deleting Card : ${error.message}`, { variant: 'error' });
    }
  }

return (
  <>

    <Box key={card.id} sx={{
      ml: 3, marginBottom: 2, width: 250,
      borderRadius: '5px', backgroundColor: '#455a64', color: 'white', fontWeight: 600, height: 30, paddingTop: '10px', paddingLeft: '5px', cursor: 'pointer'
    }}
      onClick={() => setOpen(true)} >{card.name}

    </Box>
    <Modal
      sx={{ overflowX: "auto" }}
      open={open}
      onClose={() => setOpen(false)}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <Box className="div" sx={{ display: 'flex', justifyContent: 'space-between', }}>
          Card Name :  {card.name}
          <Typography id="transition-modal-title" variant="h6" component="h2" >
            <Button variant="contained" onClick={()=>deleteCardbyid(card.id)}>
              Delete
            </Button>
          </Typography>
        </Box>
        <Checklist cardid={card.id} />
      </Box>
    </Modal>
  </>
)
}




