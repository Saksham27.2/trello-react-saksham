import React, { useState, useEffect } from 'react'
import Box from '@mui/material/Box';
import { Popover, Typography,Snackbar } from '@mui/material';
import Dialog from './Dialog'
import Board from './Board';
import CircularProgress from '@mui/material/CircularProgress';
import {  useSnackbar } from 'notistack';
import { getBoardsData } from './Api/Api';



 
export default function Boards() {
  
  const { enqueueSnackbar } = useSnackbar();

  const key = 'f7272aabb47ef3db918f01e3548289c0';
  const token = 'ATTAd5ec94d3d578fe2ee3b4a669a8e39b35ed8ce3ef461fce50dd51b0cd7c255791BFDBC51A';
  const [boards, setBoards] = useState([]);
  const [isLoading,setisloading]=useState(true);
  const [error,setError]=useState(null)



  const handleCloseErrorSnackbar = () => {
    setError(null);
  };


  useEffect(() => {
    // Fetch boards from Trello API
    const fetchBoards = async () => {
      try {
        const response =await getBoardsData()
        setBoards(response);
        setisloading(false);
      } catch (error) {
        setisloading(false);
        enqueueSnackbar(`Failed to fetch boards : ${error.message} `, { variant:'error' });
      }
    };

    fetchBoards();
  }, []);



  return (
    <>
      <Typography variant='h5' sx={{ color: '#b0bec5', fontWeight: 600, ml: 3, mt: 10 }} >
        Your Boards
      </Typography>
      <Box sx={{ backgroundColor: '#37474f', height: 'auto', mt: 4, display: 'flex', flexWrap: 'wrap', width: '92vw' }}>
        <Dialog boards={boards} setBoards={setBoards} />
        {isLoading ? (
          <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: 300 }}>
            <CircularProgress />
          </Box>
        ) : (
          boards.map((board) => <Board  board={board} setBoards={setBoards} />)
        )}

      </Box>


    </>
  )
}
