  import { Box, Typography } from '@mui/material'
  import React, { useEffect, useState } from 'react'
  import AddIcon from '@mui/icons-material/Add';
  import Button from '@mui/material/Button';
  import CloseIcon from '@mui/icons-material/Close';
  import axios from 'axios';
  import Carddialog from './Carddialog';
  import {  useSnackbar } from 'notistack';

  export default function Card({ listid }) {
    const [cards, setcards] = useState([]);
    const [addingNewcard, setAddingNewcard] = useState(false);
    const [newcardName, setNewcardName] = useState('');
    const { enqueueSnackbar } = useSnackbar();
  
    const key = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
    const token = import.meta.env.VITE_REACT_APP_TRELLO_TOKEN;
    useEffect(() => {
      const fetchCards = async () => {
        try {
          const response = await fetch(`https://api.trello.com/1/lists/${listid}/cards?key=${key}&token=${token}`)
          const data = await response.json();
          setcards(data);
        } catch (error) {
          console.log('Error in getting cards ',error);
          enqueueSnackbar(`Failed to  fetching Cards : ${error.message}`, { variant: 'error' });
        }
       
      }
      fetchCards();
    }, [])
    const handleInputCardChange = (event) => {
      console.log(event.target.value);
      setNewcardName(event.target.value);
    };
    const handleAddcard = () => {
      setAddingNewcard(false);
      setNewcardName(''); // Clear input field
    };
    const handleCardsubmit = async (e) => {
      e.preventDefault();
      try {
        const response = await axios.post(`https://api.trello.com/1/cards?key=${key}&token=${token}&name=${newcardName}&idList=${listid}`)
        console.log('Card Created Successfully');
        setAddingNewcard(false);
        setcards([...cards, response.data]);
        setNewcardName('')
      }
      catch (err) {
        enqueueSnackbar(`Failed to  Adding Cards : ${err.message}`, { variant: 'error' });
      }
    }
    const handleAddcardClick = () => {
      setAddingNewcard(true);
    };
    return (
      <Box >

        {
          cards.map((card) => (
            <Carddialog card={card } key={card.id} setcards={setcards}  />
          ))
        }
        <Box sx={{ display: 'flex' }}>
          {addingNewcard ? (
            <form onSubmit={handleCardsubmit} style={{ display: 'flex', flexDirection: 'column', backgroundColor: '#cfd8dc', marginLeft: '2rem', width: '200px' }}>
              <input
                type="text"
                value={newcardName}
                onChange={handleInputCardChange} placeholder="Enter Card title"
                style={{ width: '11rem', height: '2rem', color: 'black', border: 'none', padding: '5px', borderRadius: '10px', marginBottom: '5px' }}
              />
              <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Button variant="contained" sx={{ color: '#fff', width: 150 }}type='submit' >
                  Add Card
                </Button>
                <CloseIcon sx={{ cursor: 'pointer', color: '#607d8b' }} onClick={handleAddcard} />
              </Typography>
            </form>) : (
            <Button
              variant="contained"
              sx={{
                fontSize: '100', width: 250, mt: 2, color: '#fff',ml:3
              }}
              onClick={handleAddcardClick}
            >
              <AddIcon />Add another Card
            </Button>
          )}
        </Box>
      </Box>
    )
  }
