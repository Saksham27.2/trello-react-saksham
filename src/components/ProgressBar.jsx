import { Box, Typography } from '@mui/material'
import React from 'react'
import LinearProgress from '@mui/material/LinearProgress';


export default function ProgressBar({handleProgress}) {
  return (
    <Box sx={{ width: '100%', marginTop: 2 }}>
    <LinearProgress value={handleProgress()} variant='determinate' />
    <Typography variant='h8' sx={{ marginLeft: '0.5rem' }}>
      {`${handleProgress()}%`}
    </Typography>
  </Box>
  )
}
