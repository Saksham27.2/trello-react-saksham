import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Link } from 'react-router-dom';


export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" sx={{backgroundColor:'#263238'}}>
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 1,color:'white' }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h5" component="div" sx={{fontWeight:700}}>
            Trello
          </Typography>
          <Button sx={{color:'white'}}  component={Link} to='/boards' >Boards</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

