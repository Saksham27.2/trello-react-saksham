import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import { Box, Popover, Typography } from '@mui/material';
import Background from '../assets/Background2.jpg'
import { deleteBoard } from './Api/Api';
import { useSnackbar } from 'notistack';

export default function Board({ board, setBoards }) {

  const { enqueueSnackbar } = useSnackbar();
  const [anchorEl, setAnchorEl] = useState(null);
  const handleMoreHorizIconClick = (event) => {
    setAnchorEl(event.currentTarget);

  };

  const handleClosePopover = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);


  const deleteboardbyid = async (id) => {
    try {
      await deleteBoard(id);
      setBoards(prev => prev.filter(prev => prev.id !== id));

    }
    catch (error) {
      enqueueSnackbar(`Failed to delete board : ${error.message} `, { variant:'error' });
    }
  }
  return (
    <Box  sx={{
      width: 400, height: 100, mt: 3, ml: 4, display: 'flex', color: 'white', fontWeight: 600,

      backgroundImage: `url(${Background})`,
      borderRadius: 5,
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      opacity: 0.7,
      justifyContent: 'space-around', alignItems: 'center', textAlign: 'center'
    }}>
      <Link key={board.id} to={`/boards/${board.id}`} style={{ display: 'inline-block', textDecoration: 'none' }}>
        <Typography variant='h5' sx={{ color: 'White', fontWeight: 600, ml: 3 }}>
          {board.name}
        </Typography>
      </Link>
      <MoreHorizIcon sx={{ cursor: 'pointer', color: 'white' }} onClick={handleMoreHorizIconClick} />
      <Popover
        open={open}
        anchorEl={anchorEl}
        onClose={handleClosePopover}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}

      >
        {/* Content of the Popover */}
        <Box sx={{ p: 2 }}>
          <Typography onClick={() => {
            handleClosePopover()
            deleteboardbyid(board.id)
          }} sx={{ cursor: 'pointer' }}>Delete Board</Typography>
        </Box>
      </Popover>
    </Box>
  )
}
