import axios from "axios";

const key = import.meta.env.VITE_REACT_APP_TRELLO_API_KEY;
const token = import.meta.env.VITE_REACT_APP_TRELLO_TOKEN;
const Url = "https://api.trello.com/1/";

axios.defaults.params = {
  key: key,
  token: token,
};

export const addingBoards = async (boardName) => {
  const response = await axios.post("https://api.trello.com/1/boards", {
    name: boardName,
  });
  return response.data;
};

export const getBoardsData = async () => {
  const response = await axios.get(
    "https://api.trello.com/1/members/me/boards"
  );
  return response.data;
};

export const deleteBoard = async (boardId) => {
  const response = await axios.delete(`${Url}boards/${boardId}`);
  return response;
};

export const deleteCard = async (cardId) => {
  const response = await axios.delete(`${Url}cards/${cardId}`);
  return response;
};
export const postingList = async (id, ListVal) => {
    const res = await axios.post(
      `https://api.trello.com/1/boards/${id}/lists?name=${ListVal}`
    );
    return res.data;
  };

export const getLists = async (boardId) => {
  const response = await axios.get(`${Url}boards/${boardId}/lists`);
  return response;
};

export const deleteList = async (listId) => {
  const response = await axios.put(`${Url}lists/${listId}/closed?value=true`);
  return response;
};

export const fetchCheckList = async (selectedId) => {
  const res = await axios.get(
    `https://api.trello.com/1/cards/${selectedId}/checklists`
  );
  return res.data;
};

export const handlingDeleteCheckList = async (id) => {
  await axios.delete(`https://api.trello.com/1/checklists/${id}`);
};

export const addingCheckList = async (selectedId, enterd) => {
  const response = await axios.post(
    `https://api.trello.com/1/cards/${selectedId}/checklists?key=${key}&token=${token}&name=${enterd}`
  );
  return response.data;
};


export const fetchCheckItem = async (id) => {
    const res = await axios.get(
      `https://api.trello.com/1/checklists/${id}/checkItems?`
    );
    return res.data;
  };
  
  export const addingItem = async (id, newItem) => {
    const response = await axios.post(
      `https://api.trello.com/1/checklists/${id}/checkItems?name=${newItem}`
    );
    return response.data;
  };
  
  export const handlingDeleteItem = async (id, itemId) => {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${id}/checkItems/${itemId}`
    );
    return response.data;
  };

  export const toggler = async (selectedId, itemId, State) => {
    const res = await axios.put(
      `https://api.trello.com/1/cards/${selectedId}/checkItem/${itemId}?key=${key}&token=${token}&state=${State}`
    );
  };
