import { Box, Popover, Typography } from '@mui/material'
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';
import CircularProgress from '@mui/material/CircularProgress';
import Lists from './Lists';
import { useSnackbar } from 'notistack';
import { getLists,postingList } from './Api/Api';
export default function List() {
    const { boardId } = useParams();
    const [lists, setlists] = useState([]);
    const [newListName, setNewListName] = useState('');
    const [addingNewList, setAddingNewList] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);
    const [isLoading, setisloading] = useState(true);
    const { enqueueSnackbar } = useSnackbar();

    const fetchlists = async () => {
        try {
            const response = await getLists(boardId);
            setlists(response.data);
            setisloading(false)
        }
        catch (error) {
            setisloading(false)
            enqueueSnackbar(`Failed to  Fetch Lists : ${error.message}`, { variant: 'error' });
        }
    }
    useEffect(() => {
        fetchlists();
    }, [])

    const handleAddListClick = () => {
        setAddingNewList(true);
    };

    const handleInputChange = (event) => {
        setNewListName(event.target.value);
    };
    const handleAddList = () => {
        setAddingNewList(false);
        setNewListName(''); 

    };

    const handlesubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await postingList(boardId,newListName)
            console.log('List Created Successfully');
            setAddingNewList(false);
            setlists(prev => [...prev, response]);
            setNewListName('');
        }
        catch (err) {
            console.log('Error in creating List - >', err);
        }
    }


    return (
        <Box >
            <Typography variant='h4' sx={{ mt: 12, color: 'white', ml: 4 }}>
                Lists
            </Typography>

            <Box sx={{ display: 'flex', minWidth: '300px', overflowX: 'auto' }}>
                <Box>
                    {addingNewList ? (
                        <form onSubmit={handlesubmit} style={{ display: 'flex', flexDirection: 'column', backgroundColor: '#cfd8dc', marginTop: '1.5rem', marginLeft: '1rem', width: '200px', paddingBottom: 1, height: 150, borderRadius: 5 }}>
                            <input
                                type="text"
                                value={newListName}
                                onChange={handleInputChange}
                                placeholder="Enter list name"
                                style={{ marginTop: '1rem', width: '11rem', marginLeft: '0.5rem', height: '2rem', borderRadius: 5, border: 'none', paddingLeft: '10px' }}
                            />
                            <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                <Button
                                    variant="contained"
                                    sx={{ color: '#fff', mt: 4, ml: 1 }}
                                    type='submit'
                                >
                                    Add List
                                </Button>
                                <CloseIcon sx={{ mt: 2, cursor: 'pointer' }} onClick={handleAddList} />
                            </Typography>
                        </form>
                    ) : (
                        <Button
                            variant="contained"
                            sx={{
                                backgroundColor: '#263238', fontSize: '100', width: 200, ml: 4, mt: 3, color: '#fff',
                                '&:hover': {
                                    backgroundColor: '#cfd8dc',
                                    color: 'black',
                                },
                            }}
                            onClick={handleAddListClick}
                        >
                            <AddIcon />Add another List
                        </Button>
                    )}
                </Box>
                {
                    isLoading ? (<Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: 300 }}>
                        <CircularProgress />
                    </Box>) :
                        lists.map((list) => (
                            <Lists list={list} setlists={setlists} />
                        ))
                       
                }

            </Box>
        </Box>
    )
}
